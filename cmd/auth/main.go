package main

import (
	"fmt"
	"time"

	jwt "github.com/golang-jwt/jwt"

	"gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app/pkg/database"
	"gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app/pkg/system"
	"gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app/pkg/types"
)

var (
	jwtAlg *jwt.SigningMethodHMAC = jwt.SigningMethodHS256
)

type Auth struct {
	SystemManager *system.SystemManager
}

func (a *Auth) GenerateJWTauthToken(id string, authNonce string, expiresIn time.Duration) (tokenString string, err error) {
	if expiresIn == 0 {
		expiresIn = 24 * 5
	}
	secret, err := a.SystemManager.GetJWTsecret()
	if err != nil {
		return "", err
	}
	expirationTime := time.Now().Add(time.Hour * expiresIn)
	token := jwt.NewWithClaims(jwtAlg, types.JWTclaim{
		ID:        id,
		AuthNonce: authNonce,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	})

	tokenString, err = token.SignedString([]byte(secret))
	return tokenString, err
}

func main() {
	runtime := &Auth{}
	systemManager := &system.SystemManager{}
	db, err := database.NewDatabase().Open()
	if err != nil {
		panic(err)
	}
	systemManager.DB = db
	runtime.SystemManager = systemManager

	t, err := runtime.GenerateJWTauthToken("1", "2", time.Duration(1*time.Second))
	if err != nil {
		panic(err)
	}
	fmt.Println("Token:", t)
}
