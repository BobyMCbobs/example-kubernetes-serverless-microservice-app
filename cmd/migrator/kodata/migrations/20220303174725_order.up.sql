begin;

create table if not exists order (
  id text default md5(random()::text || clock_timestamp()::text)::uuid not null,
  comment text not null,
  userid text not null,
  drinkid text not null,
  fulfilled boolean not null default false,
  creationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  modificationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  deletionTimestamp int not null default 0,

  primary key (id),
  foreign key (userid) references user(id),
  foreign key (drinkid) references product(id)
);

comment on table user is 'The order table for storing order of products';

commit;
