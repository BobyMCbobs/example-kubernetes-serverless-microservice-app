package main

import (
	"log"
	"os"
	"time"

	// include Pg
	_ "github.com/lib/pq"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app/pkg/common"
	"gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app/pkg/database"
)

func main() {
	log.Printf("migrate starting")
	canConnect := false
	for canConnect == false {
		_, err := database.NewDatabase().Open()
		if err != nil {
			log.Println("unable to connect to database. Waiting...")
			time.Sleep(time.Second * 5)
			continue
		}
		canConnect = true
	}

	m, err := migrate.New(
		"file:///var/run/ko/migrations",
		common.GetAppDatabaseConnectionString())
	if err != nil {
		log.Printf("migrate failed: %v", err)
		os.Exit(1)
	}
	m.Up()
	log.Printf("migrate complete")
}
