module gitlab.com/BobyMCbobs/example-kubernetes-serverless-microservice-app

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/lib/pq v1.10.0
)
