package system

import (
	"database/sql"
)

type SystemManager struct {
	DB *sql.DB
}

func (s SystemManager) getValue(name string) (output string, err error) {
	sqlStatement := `select value from system where name = $1`
	rows, err := s.DB.Query(sqlStatement, name)
	if err != nil {
		return output, err
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&output)
	}
	return output, err
}

func (s SystemManager) setValue(name, value string) (err error) {
	sqlStatement := `update system set value = $2 where name = $1`
	rows, err := s.DB.Query(sqlStatement, name, value)
	defer rows.Close()
	return err
}

func (s SystemManager) GetJWTsecret() (string, error) {
	return s.getValue("jwtSecret")
}
