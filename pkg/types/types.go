package types

import (
	jwt "github.com/golang-jwt/jwt"
)

type JWTclaim struct {
	ID        string `json:"id"`
	AuthNonce string `json:"authNonce"`
	jwt.StandardClaims
}
