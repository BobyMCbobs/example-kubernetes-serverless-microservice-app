package common

import (
	"os"
)

func GetEnvValueOrDefault(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value != "" {
		return value
	}
	return defaultValue
}

func GetAppDatabaseConnectionString() string {
	return GetEnvValueOrDefault("APP_DB_STRING",
		"postgres://postgres:postgres@postgres:5432/postgres?sslmode=disable")
}
